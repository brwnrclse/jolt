# Bison Maps
## *codename Jolt*

A mapbox powered location app for howard university


### Getting started

First install dependencies (node & npm required)

``` bash
# install dependenies
git clone bitbucket.org/brwnrclse/jolt
cd jolt
npm install
npm run jspm:install
```

### Dev Work
Next to get things running use (start, start:dev, start:watch)

1. start does normal bundling and deployment

    ``` bash
    npm start
    ```

2. start:dev runs browser-sync and systemjs-hotreloader

    ``` bash
    npm start:dev
    ```

3. tart:watch runs start:dev as well as babel watch for reloading on server-side routes and server file. 

    ``` bash
    npm run start:watch
    ```
