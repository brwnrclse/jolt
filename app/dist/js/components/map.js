import React, {Component} from 'react';

import pacomoTransform from '../utils/pacomo.js';

/**
 * Our mapbox component for directions/viewing
 *
 * @return     {React.Component} a vdom representation of our component
 */
const MapComponent = () => {
  return (
    <div>
      <div id={`jolt-content-Map`} />
    </div>
  );
};

MapComponent.displayName = `content-Map`;

export default pacomoTransform(MapComponent);
