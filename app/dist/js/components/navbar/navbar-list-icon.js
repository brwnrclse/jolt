import React, {Component, PropTypes} from 'react';

import pacomoTransformer from '../../utils/pacomo.js';

/**
 * Navbar List Icon Item
 *
 * @param      {Object} props
 * @param      {string} props.description alt property of image
 * @param      {string} props.uri URL location of image
 * @return     {React.Component} a vdom representation of or component
 */
const NavbarListIcon = ({description, uri}) => {
  return (
    <li>
      <img
          alt={description}
          className={`icon`}
          src={uri}
      />
    </li>
  );
};

NavbarListIcon.displayName = `Navbar-List-Icon`;
NavbarListIcon.propTypes = {
  description: PropTypes.string.isRequired,
  uri: PropTypes.string.isRequired
};

export default pacomoTransformer(NavbarListIcon);
