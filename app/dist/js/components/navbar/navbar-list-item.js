import React, {Component, PropTypes} from 'react';

import pacomoTransformer from '../../utils/pacomo.js';

/**
 * Navbar List Item
 *
 * @param      {Object} props
 * @param      {string} props.content What we want to display
 * @return     {React.Component} a vdom representation of or component
 */
const NavbarListItem = ({content}) => {
  return (
    <li>
      <h1 className={`content`}>
        {content}
      </h1>
    </li>
  );
};

NavbarListItem.displayName = `Navbar-List-Item`;
NavbarListItem.propTypes = {
  content: PropTypes.string.isRequired
};

export default pacomoTransformer(NavbarListItem);
