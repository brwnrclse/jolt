import React, {Component} from 'react';

import NavBarListIcon from './navbar-list-icon.js';
import NavBarListItem from './navbar-list-item.js'
import pacomoTransformer from '../../utils/pacomo.js';

/**
 * Navbar List component
 *
 * @return     {React.Component} vdom representation of our component
 */
const NavbarList = () => {
  return (
    <ul>
      <NavBarListItem content={`Bison`} />
      <NavBarListIcon
          description={`Howard University bison logo`}
          uri={`assets/img/bison.svg`}
      />
      <NavBarListItem content={`Maps`} />
    </ul>
  )
};

NavbarList.displayName = `Navbar-List`;

export default pacomoTransformer(NavbarList);
