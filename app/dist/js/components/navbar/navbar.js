import React, {Component} from 'react';

import NavbarList from './navbar-list.js';
import pacomoTransformer from '../../utils/pacomo.js';

/**
 * Navbar Component
 *
 * @return     {React.Component} vdom representation of our Component
 */
const Navbar = () => {
  return (
    <nav>
      <NavbarList />
    </nav>
  )
};

Navbar.displayName = `Navbar`;

export default pacomoTransformer(Navbar);
