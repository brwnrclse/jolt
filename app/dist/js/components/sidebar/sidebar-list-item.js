import React, {Component, PropTypes} from 'react';

import pacomoTransformer from '../../utils/pacomo.js';

/**
 * Component for display location details in the sidebar
 *
 * @param      {Object} props
 * @param      {string} props.description
 * @param      {string} props.title
 * @return     {React.Component} vdom representation for our component
 */
const SidebarListItem = ({description, title}) => {
  return (
    <li>
      <h1 className={`H1`}>{title}</h1>
      <p className={`text`}>{description}</p>
      <span className={`SubText`}>{`< 1 mile away`}</span>
    </li>
  )
};

SidebarListItem.displayName = `content-Sidebar-ListItem`;
SidebarListItem.propTypes = {
  description: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired
};

export default pacomoTransformer(SidebarListItem);
