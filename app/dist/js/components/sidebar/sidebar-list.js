import React, {Component, PropTypes} from 'react';

import pacomoTransformer from '../../utils/pacomo.js';
import SidebarListItem from './sidebar-list-item.js';

/**
 * A List container, for the sidebar
 *
 * @param      {Object} props
 * @param      {Object[] props.items
 * @return     {React.Components} vdom representation of our component
 */
const SidebarList = ({items}) => {
  return (
    <ul>
      {items.map((x, i) => {
        return (
          <SidebarListItem
              description={x.description}
              key={i}
              title={x.title}
          />
        );
      })}
    </ul>
  );
};

SidebarList.displayName = `content-Sidebar-List`;
SidebarList.propTypes = {
  items: PropTypes.array.isRequired
};

export default pacomoTransformer(SidebarList);
