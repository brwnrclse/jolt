import React, {Component, PropTypes} from 'react';

import pacomoTransformer from '../../utils/pacomo.js';
import SidebarList from './sidebar-list.js';

/**
 * Sidebar component
 *
 * @param      {Object} props
 * @param      {Object[]} props.items Items to render in the sidebar
 * @return     {React.Component} vdom representation of our component
 */
const Sidebar = ({items}) => {
  return  (
    <div>
        <SidebarList items={items} />
    </div>
  );
};

Sidebar.displayName = `content-Sidebar`;
Sidebar.propTypes = {
  items: PropTypes.array.isRequired
};

export default pacomoTransformer(Sidebar);
