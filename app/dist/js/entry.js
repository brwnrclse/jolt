import React from 'react';
import {render} from 'react-dom';
import {browserHistory} from 'react-router';

import Jolt from './utils/router.js';

const places = [
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  },
  {
    'title': `Louis K. Downing Bldg`,
    'description': `LKD, The Engineering building`
  }
];

render(React.createElement(Jolt, {history: browserHistory, homePlaces: places}),
    document.querySelector(`#jolt-root`));
