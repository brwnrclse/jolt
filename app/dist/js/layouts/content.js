import React, {Component, PropTypes} from 'react';

import MapComponent from '../components/map.js';
import pacomoTransfromer from '../utils/pacomo.js';
import Sidebar from '../components/sidebar/sidebar.js';

/**
 * Layout for the app main container
 *
 * @param      {Object} props
 * @param      {Object} props.places
 * @return     {React.Component} vdom representation of our app
 */
const Content = ({places}) => {
  return (
    <div>
      <Sidebar items={places}/>
      <MapComponent />
    </div>
  );
};

Content.displayName = `content`;
Content.propTypes = {
  places: PropTypes.array.isRequired
};

export default pacomoTransfromer(Content);
