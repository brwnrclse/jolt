import React, {Component, PropTypes} from 'react';

import Content from './content.js';
import Navbar from '../components/navbar/navbar.js';
import pacomoTransformer from '../utils/pacomo.js';

/**
 * Simple container for our Home layout.
 *
 * @return     {React.Component}  A vdom representation of our component
 */
const Home = ({places}) => {
  return (
    <div>
      <Navbar />
      <Content places={places} />
    </div>
  );
};

Home.displayName = `Home`;
Home.propType = {
  places: PropTypes.array.isRequired
}

export default pacomoTransformer(Home);
