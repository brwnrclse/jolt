import {withPackageName} from 'react-pacomo';

const {transformer: pacomoTransformer} = withPackageName('jolt');

export default pacomoTransformer;
