import React, {Component, PropTypes} from 'react';
import {Redirect, Route, Router} from 'react-router';

import Home from '../layouts/home.js';
import pacomoTransformer from './pacomo.js';

/**
 * A utility factory for creating our router component
 *
 * @param      {Object}
 * @param      {Object}   props.history react-router browserHistory
 * @return     {React.Component}  react-router component
 */
const Routes = ({history, homePlaces}) => {
  const HomeWrapper = () => {
    return React.createElement(Home, {places: homePlaces});
  };

  return (
    <Router history={history}>
      <Route path={`/`} component={HomeWrapper} />
      <Redirect from={`*`} to={`/`} />
    </Router>
  );
};

Routes.displayName = `Routes`;
Routes.propTypes = {
  history: PropTypes.object.isRequired,
  homePlaces: PropTypes.array.isRequired
};

export default Routes;
