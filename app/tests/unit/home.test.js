import addAssertions from 'extend-tape';
import createComponent from 'react-unit';
import {createRenderer} from 'react-addons-test-utils';
import jsxEquals from 'tape-jsx-equals';
import React from 'react';
import tape from 'tape';

import Home from '../../dist/js/layouts/home.js';

const test = addAssertions(tape, {jsxEquals});

test(`----- Jolt React Component Test: Home -----`, (swear) => {
  const
    swearJar = 3,
    imagineComponentClassName = `jolt-Home`,
    imagineH1Text = `Hello Jolt!`,
    imagineJSX = (
      <div className={`jolt-Home`}>
        <h1>{`Hello Jolt!`}</h1>
      </div>
    ),
    bonafideComponent = createComponent.shallow(<Home />),
    bonafideH1 = bonafideComponent.findByQuery(`h1`)[0],
    renderer = createRenderer();

  swear.plan(swearJar);
  swear.comment(`Home component should have correct props`);
  swear.equal(bonafideComponent.props.className, imagineComponentClassName,
      `renders w/ correct prop -- className`);
  swear.equal(bonafideH1.text, imagineH1Text,
      `renders w/ correct child prop -- text`);
  swear.comment(`Home component should render correctly`);
  renderer.render(<Home />);
  swear.jsxEquals(renderer.getRenderOutput(), imagineJSX,
      `renders jsx correctly`);
});
