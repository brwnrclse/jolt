import addAssertions from 'extend-tape';
import createComponent from 'react-unit';
import {createRenderer} from 'react-addons-test-utils';
import jsxEquals from 'tape-jsx-equals';
import React from 'react';
import tape from 'tape';

const test = addAssertions(tape, {jsxEquals});

test(`----- Jolt React Component Test: Layout -----`, (swear) => {
  const swearJar = 1;

  swear.plan(swearJar);
  swear.ok(true, `No tests yet!`);
});
